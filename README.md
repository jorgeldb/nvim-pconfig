# Prerequisites
- neovim (of course)
- fd
- ripgrep
- lazygit
- nodejs
- npm
- make
- gcc
- g++

**Note**: On windows, ncat from nmap is required for the gdscript server. Standard netcat (nc) does not work for magical reasons.

# Istallation
1. Clone the git repository on the machine

    **Note**: Make sure to backup your config if you already have one.

    <details>
        <summary>Using https</summary>

    Powershell:
    ```powershell
    git clone https://gitlab.com/jorgeldb/nvim-pconfig.git $env:localappdata\nvim
    ```

    Cmd:
    ```cmd
    git clone https://gitlab.com/jorgeldb/nvim-pconfig.git %localappdata%\nvim
    ```

    Sh:
    ```bash
    git clone https://gitlab.com/jorgeldb/nvim-pconfig.git ~/.config/nvim
    ```

    </details>
    <details>
        <summary>Using ssh</summary>

    **Note**: This only works for me, but I left it here for (my) convenience.

    Powershell:
    ```powershell
    git clone git@gitlab.com:jorgeldb/nvim-pconfig.git $env:localappdata\nvim
    ```

    Cmd:
    ```cmd
    git clone git@gitlab.com:jorgeldb/nvim-pconfig.git %localappdata%\nvim
    ```

    Sh:
    ```bash
    git clone git@gitlab.com:jorgeldb/nvim-pconfig.git ~/.config/nvim
    ```

    </details>

2. Open neovim

3. Use it
