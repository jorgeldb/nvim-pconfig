return {
  {
    "folke/noice.nvim",
    lazy = true,
    event = "VeryLazy",
    dependencies = {
      "MunifTanjim/nui.nvim",
    },
    opts = {
      cmdline = {
        enabled = false,
      },
      messages = {
        enabled = false,
      },
      notify = {
        enabled = false,
      },
      popupmenu = {
        enabled = false,
      },
      lsp = {
        hover = {
          enabled = false,
        },
        diagnostic = {
          enabled = false,
        },
        signature = {
          auto_open = {
            enabled = false,
          },
        },
      },
    },
  },
  {
    "nvim-lualine/lualine.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    lazy = true,
    event = "VeryLazy",
    config = function()
      require("lualine").setup({
        options = {
          section_separators = { left = "", right = "" },
          component_separators = { left = "|", right = "|" },
        },
        sections = {
          lualine_a = { "mode" },
          lualine_b = { "branch", "diff", "diagnostics" },
          lualine_c = { { "filename", path = 1, symbols = { modified = "●", readonly = "" } } },
          lualine_x = { "encoding", "fileformat", "filetype" },
          lualine_y = { "progress" },
          lualine_z = { "location" },
        },
        inactive_sections = {
          lualine_a = {},
          lualine_b = {},
          lualine_c = { { "filename", path = 1, symbols = { modified = "●", readonly = "" } } },
          lualine_x = { "filetype", "location" },
          lualine_y = {},
          lualine_z = {},
        },
      })
    end,
  },
  {
    "stevearc/dressing.nvim",
    lazy = true,
    event = "VeryLazy",
    opts = {
      input = {
        insert_only = false,
        start_in_insert = false,
      },
    },
  },
}
