local keymaps = require("jorge.config.keymaps")
return {
  {
    "hrsh7th/nvim-cmp",
    lazy = true,
    version = false, -- last release is way too old
    event = "InsertEnter",
    dependencies = {
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-nvim-lsp-signature-help",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "saadparwaiz1/cmp_luasnip",
    },
    opts = function()
      local cmp = require("cmp")
      return {
        completion = {
          completeopt = "menu,menuone,noinsert,noselect",
        },
        snippet = {
          expand = function(args)
            require("luasnip").lsp_expand(args.body)
          end,
        },
        mapping = keymaps.get_lsp_mappings(),
        sources = cmp.config.sources({
          { name = "nvim_lsp" },
          { name = "luasnip" },
          { name = "nvim_lsp_signature_help" },
          { name = "orgmode" },
          { name = "buffer" },
          { name = "path" },
        }),
        window = {
          completion = {
            border = { "╔", "═", "╗", "║", "╝", "═", "╚", "║" },
          }
        }
      }
    end,
  },
  {
    "L3MON4D3/LuaSnip",
    lazy = true,
    event = "InsertEnter",
    build = (not jit.os:find("Windows"))
        and "echo -e 'NOTE: jsregexp is optional, so not a big deal if it fails to build\n'; make install_jsregexp"
        or nil,
    dependencies = {
      "rafamadriz/friendly-snippets",
      config = function()
        require("luasnip.loaders.from_vscode").lazy_load()
      end,
    },
    config = function()
      local lsnip = require("luasnip")
      local snippets = require("jorge.plugins.lsp.snippets").get_snippets()
      keymaps.set_luasnip_keymaps()
      lsnip.add_snippets(nil, snippets)
      lsnip.setup({
        history = true,
        delete_check_events = "TextChanged",
      })
    end,
  },
  {
    "neovim/nvim-lspconfig",
    lazy = true,
    event = { "BufReadPre", "BufNewFile" },
    cmd = { "LspInfo" },
    dependencies = {
      "hrsh7th/cmp-nvim-lsp",
      { "folke/neodev.nvim", opts = {} },
      "nvimtools/none-ls.nvim",
    },
    config = function()
      require("lspconfig.ui.windows").default_options = { border = "double" }
      local jorge_servers = require("jorge.plugins.lsp.servers")
      jorge_servers.setup_servers()
    end,
  },
}
