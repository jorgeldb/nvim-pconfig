local M = {}

M.get_snippets = function()
  local lsnip = require("luasnip")
  return {
    go = {
      lsnip.snippet("err", {
        lsnip.text_node({ "if err != nil {", "\t" }),
        lsnip.insert_node(1, "code"),
        lsnip.text_node({ "", "}" }),
      }),
    },
    python = {
      lsnip.snippet("main", {
        lsnip.text_node({ "def main():", "    " }),
        lsnip.insert_node(1, "pass"),
        lsnip.text_node({"", "", "if __name__ == \"__main__\":", "    main()"}),
      }),
    },
  }
end

return M
