local M = {}
local keymaps = require("jorge.config.keymaps")
local cmp_nvim_lsp = require("cmp_nvim_lsp")
local general_capabilities = cmp_nvim_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities())
local general_on_attach = function()
  keymaps.set_lsp_keymaps()
end

local gdscript_nc_command = "nc"
if vim.loop.os_uname().sysname == "Windows_NT" then
  gdscript_nc_command = "ncat"
end

-- Servers installed Manually
M.server_opts_lsp = {
  -- C/C++
  clangd = {
    cmd = {
      "clangd",
      "--offset-encoding=utf-16",
    },
    autostart = false,
  },
  ccls = {
    autostart = true,
  },

  -- C#
  csharp_ls = {
    autostart = true,
  },

  -- Godot engine
  gdscript = {
    cmd = { gdscript_nc_command, "127.0.0.1", "6005" },
    autostart = true,
  },

  -- Gleam
  gleam = {},

  -- Go
  gopls = {
    autostart = true,
  },

  -- Java
  jdtls = {
    autostart = true,
  },

  -- Lua
  lua_ls = {
    capabilities = vim.tbl_deep_extend("force", general_capabilities, { { textDocument = { formatting = false } } }),
    autostart = true,
    settings = {
      Lua = {
        diagnostics = {
          globals = { "vim" },
        },
        workspace = {
          checkThirdParty = false,
          library = {
            [vim.fn.expand("$VIMRUNTIME/lua")] = true,
            [vim.fn.stdpath("config") .. "/lua"] = true,
          },
        },
        completion = {
          callSnippet = "Replace",
        },
      },
    },
  },

  -- JSON
  jsonls = {
    autostart = true,
  },

  -- Haskell
  hls = {
    autostart = true,
  },

  -- Ocaml
  ocamllsp = {
    autostart = true,
  },

  -- Python
  pyright = {
    autostart = true,
  },
  ruff = {
    capabilities = vim.tbl_deep_extend("force", general_capabilities, { { textDocument = { completion = false } } }),
  },

  -- Javascript / Typescript / React / HTML
  quick_lint_js = {
    autostart = true,
  },
  ts_ls = {
    autostart = true,
    capabilities = vim.tbl_deep_extend("force", general_capabilities, { { textDocument = { formatting = false } } }),
  },
  eslint = {
    autostart = true,
    settings = {
      format = false,
    },
  },
  emmet_language_server = {
    autostart = true,
  },

  -- Rust
  rust_analyzer = {
    autostart = true,
    settings = {
      ["rust-analyzer"] = {
        imports = {
          granularity = {
            group = "module",
          },
          prefix = "self",
        },
        cargo = {
          buildScripts = {
            enable = true,
          },
        },
        procMacro = {
          enable = true,
        },
        diagnostics = {
          enable = true,
          experimental = {
            enable = true,
          },
        },
      },
    },
  },

  -- Toml
  taplo = {
    autostart = true,
  },
  -- Zig
  zls = {
    settings = {
      enable_autofix = false,
    },
  },
}

local has_executable = function(cmd)
  local t = type(cmd)
  if t == "table" then
    return vim.fn.executable(cmd[1]) == 1
  elseif t == "string" then
    return vim.fn.executable(cmd) == 1
  elseif t == "function" then
    return true
  end
  return false
end

M.setup_servers = function()
  for server, server_opts in pairs(M.server_opts_lsp) do
    server_opts.on_attach = server_opts.on_attach or general_on_attach
    server_opts.capabilities = server_opts.capabilities or general_capabilities
    if server_opts.autostart == nil then
      server_opts.autostart = false
    end

    local notify = vim.notify

    ---@param msg string
    ---@param level integer | nil
    ---@param opts table | nil
    ---@diagnostic disable-next-line: duplicate-set-field
    vim.notify = function(msg, level, opts)
      if msg:match("warning: multiple different client offset_encodings") then
        return
      end

      notify(msg, level, opts)
    end

    local c = server_opts.cmd
    local dc = require("lspconfig")[server].document_config.default_config.cmd

    if (c and has_executable(c)) or (dc and has_executable(dc)) then
      require("lspconfig")[server].setup(server_opts)
    end
  end
end

return M
