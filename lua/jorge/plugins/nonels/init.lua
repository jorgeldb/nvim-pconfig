return {
  "nvimtools/none-ls.nvim",
  lazy = true,
  versions = false,
  opts = function()
    local nls = require("null-ls")
    local nls_clients = require("jorge.plugins.nonels.clients")
    local keymaps = require("jorge.config.keymaps")

    nls.setup({
      sources = nls_clients.clients,
      on_attach = keymaps.set_lsp_keymaps,
    })
  end,
}
