local M = {}
local nls = require("null-ls")
local helpers = require("null-ls.helpers")
local methods = require("null-ls.methods")

M.clang_format_formatter = helpers.make_builtin({
  name = "clang_format",
  meta = {
    url = "https://www.kernel.org/doc/html/latest/process/clang-format.html",
    description = "Tool to format C/C++/… code according to a set of rules and heuristics.",
  },
  method = { methods.internal.FORMATTING, methods.internal.RANGE_FORMATTING },
  filetypes = { "c", "cpp", "cuda" },
  generator_opts = {
    command = "clang-format",
    args = helpers.range_formatting_args_factory(
      { "--asume-filename", "$FILENAME" },
      "--offset",
      "--lenght",
      { use_length = true, row_offset = -1, col_offset = -1 }
    ),
    to_stdin = true,
  },
  factory = helpers.formatter_factory,
})

M.jq_formatter = helpers.make_builtin({
  name = "jq",
  meta = {
    url = "https://github.com/stedolan/jq",
    description = "Command-line JSON processor",
  },
  method = methods.internal.FORMATTING,
  filetypes = { "json" },
  generator_opts = { command = "jq", to_stdin = true },
  factory = helpers.formatter_factory,
})

-- Formatters
M.formatters = {
  nls.builtins.formatting.black, --python
  M.clang_format_formatter,
  nls.builtins.formatting.csharpier,
  nls.builtins.formatting.gofmt,
  M.jq_formatter,
  nls.builtins.formatting.ocamlformat,
  nls.builtins.formatting.prettier.with({
    filetypes = {
      "javascript",
      "javascriptreact",
      "typescript",
      "typescriptreact",
      "vue",
      "css",
      "scss",
      "less",
      "html",
      "yaml",
      "markdown",
      "markdown.mdx",
      "graphql",
      "handlebars",
    },
  }),
  nls.builtins.formatting.stylua,
}

M.clients = {}
for _, v in ipairs(M.formatters) do
  if v._opts and vim.fn.executable(v._opts.command) == 1 then
    table.insert(M.clients, v)
  end
end

return M
