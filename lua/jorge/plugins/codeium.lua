local keymaps = require("jorge.config.keymaps")

return {
  "Exafunction/codeium.vim",
  event = "BufEnter",
  config = function ()
    vim.g.codeium_manual = true
    vim.g.codeium_enabled = false
    vim.g.codeium_disable_bindings = 1
    keymaps.set_codeium_keymaps()
  end,
}
