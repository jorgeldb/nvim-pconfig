local keymaps = require("jorge.config.keymaps")
return {
  {
    "lewis6991/gitsigns.nvim",
    event = { "BufReadPre", "BufNewFile" },
    lazy = true,
    config = function(_, opts)
      require("gitsigns").setup(opts)
    end,
  },
  {
    "RRethy/vim-illuminate",
    event = { "BufReadPost", "BufNewFile" },
    lazy = true,
    opts = { delay = 200 },
    config = function(_, opts)
      require("illuminate").configure(opts)
    end,
  },
  {
    "folke/todo-comments.nvim",
    dependencies = { "nvim-lua/plenary.nvim" },
    lazy = true,
    event = "VeryLazy",
    opts = {},
  },
  {
    "lukas-reineke/indent-blankline.nvim",
    main = "ibl",
    lazy = false,
    config = function()
      require("ibl").setup({
        scope = {
          show_start = false,
          show_end = false,
        }
      })
    end,
  },
  {
    "echasnovski/mini.surround",
    version = "*",
    lazy = false,
    config = function()
      require("mini.surround").setup({
        custom_surroundings = {
          ["|"] = {
            input = { "|" },
            output = { left = "|", right = "|" },
          },
        },
        mappings = keymaps.get_surround_mappings(),
      })
      keymaps.set_surround_mappings()
    end,
  },
  {
    "jghauser/fold-cycle.nvim",
    lazy = false,
    config = function()
      require("fold-cycle").setup()
      keymaps.set_fold_keymaps()
    end
  },
}
