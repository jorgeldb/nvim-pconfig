local keymaps = require("jorge.config.keymaps")

local tterm_shell = vim.o.shell
if (vim.loop.os_uname().sysname == "Windows_NT") then
  tterm_shell = "powershell.exe"
end

return {
  "akinsho/toggleterm.nvim",
  lazy = true,
  cmd = { "ToggleTerm", "ToggleTermSendCurrentLine", "ToggleTermSendVisualLines" },
  keys = keymaps.get_term_keymaps,
  version = "*",
  config = function()
    require("toggleterm").setup({
      size = 10,
      open_mapping = [[<c-\>]],
      hide_numbers = true,
      shade_filetypes = {},
      shade_terminals = true,
      shading_factor = 2,
      start_in_insert = true,
      insert_mappings = true,
      persist_size = true,
      direction = "float",
      close_on_exit = true,
      shell = tterm_shell,
      float_opts = {
        border = "double",
        winblend = 0,
        highlights = {
          border = "Normal",
          background = "Normal",
        },
      },
    })
  end,
}
