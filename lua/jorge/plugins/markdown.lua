return {
  "richardbizik/nvim-toc",
  lazy = false,
  config = function()
    require("nvim-toc").setup({
      toc_header = "Table of Contents",
    })
  end
}
