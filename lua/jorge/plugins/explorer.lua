local run_build_command = function(plugin)
  local cwd = plugin.dir
  local command = "make"
  if vim.fn.executable("zig") == 1 then
    command = [[make CC="zig cc"]]
  end

  if vim.loop.os_uname().sysname == "Windows_NT" then
    os.execute("cd /d " .. cwd .. " && " .. command)
  else
    os.execute("cd " .. cwd .. " && " .. command)
  end
end

local keymaps = require("jorge.config.keymaps")

return {
  {
    "stevearc/oil.nvim",
    lazy = false,
    dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
      require("oil").setup({
        view_options = {
          show_hidden = true,
        },
        keymaps = keymaps.get_oil_keymaps(),
      })
    end,
  },
  {
    "nvim-telescope/telescope.nvim",
    lazy = true,
    cmd = "Telescope",
    keys = keymaps.get_telescope_keymaps(),
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-telescope/telescope-media-files.nvim",
      { "nvim-telescope/telescope-fzf-native.nvim", build = run_build_command },
    },

    config = function()
      local telescope = require("telescope")
      telescope.setup({
        defaults = {
          mappings = keymaps.get_telescope_mappings(),
          border = {},
          borderchars = { "═", "║", "═", "║", "╔", "╗", "╝", "╚" },
          file_ignore_patterns = { ".git/" },
        },
        extensions = {
          fzf = {
            fuzzy = true, -- false will only do exact matching
            override_generic_sorter = true, -- override the generic sorter
            override_file_sorter = true, -- override the file sorter
            case_mode = "smart_case", -- or "ignore_case" or "respect_case"
          },
          media_files = {},
        },
        pickers = {
          find_files = {
            push_tagstack_on_edit = true,
          },
          live_grep = {
            push_tagstack_on_edit = true,
          },
        }
      })
      telescope.load_extension("media_files")
      telescope.load_extension("fzf")
    end,
  },
}
