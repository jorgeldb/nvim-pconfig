local parsers_list = {
  "bash",
  "c",
  "cpp",
  "css",
  "c_sharp",
  "gleam",
  "go",
  "html",
  "javascript",
  "json",
  "lua",
  "luap",
  "luau",
  "markdown",
  "markdown_inline",
  "python",
  "rust",
  "typescript",
  "vimdoc",
  "yaml",
}

return {
  {
    "nvim-treesitter/nvim-treesitter",
    dependencies = { "nvim-treesitter/nvim-treesitter-context" },
    version = false, -- last release is way too old and doesn't work on Windows
    build = ":TSUpdate",
    lazy = false,
    config = function()
      require("nvim-treesitter.configs").setup({
        modules = {},
        ignore_install = {},
        auto_install = false,
        sync_install = false,
        indent = { enable = true },
        highlight = {
          enable = true,
          disable = function(_, buf)
            local max_filesize = 100 * 1024 --TODO: Find optimal filesize to disable highlighting
            local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
            if ok and stats and stats.size > max_filesize then
              return true
            end
          end,
          additional_vim_regex_highlighting = false,
        },
        context_commentstring = { enable = true, enable_autocmd = false },
        ensure_installed = parsers_list,
      })
      require("treesitter-context").setup({
        max_lines = 1
      })
    end,
  },
}
