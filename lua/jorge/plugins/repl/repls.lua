return {
  sh = {
    command = function()
      local shell = vim.opt_local.shell:get()
      if string.match(shell, "powershell") or string.match(shell, "pwsh") then
        shell = "sh"
      end
      return shell
    end,
  },
}
