local keymaps = require("jorge.config.keymaps")
local repl_definition = require("jorge.plugins.repl.repls")

return {
  "Vigemus/iron.nvim",
  lazy = true,
  event = "VeryLazy",
  config = function()
    local iron = require("iron.core")
    iron.setup({
      config = {
        repl_open_cmd = require("iron.view").split("40%"),
        repl_definition = repl_definition,
      },
      keymaps = keymaps.get_iron_keymaps(),
      highlight = {
        italic = false,
        bold = false,
      },
      ignore_blank_lines = true,
    })
  end,
}
