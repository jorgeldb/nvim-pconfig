local colorscheme = require("jorge.config.colorscheme")

return {
  {
    "EdenEast/nightfox.nvim",
    priority = 1000,
  },
  {
    "catppuccin/nvim",
    name = "catppuccin",
    priority = 1000,
  },
  {
    "ellisonleao/gruvbox.nvim",
    priority = 1000,
  },
  {
    "xiyaowong/transparent.nvim",
    lazy = false,
    priority = 1000,
    config = function()
      require("transparent").setup({
        extra_groups = {
          "NormalFloat",
        },
      })

      if colorscheme.default_transparent_state then
        vim.cmd("TransparentEnable")
      else
        vim.cmd("TransparentDisable")
      end
      vim.cmd("colorscheme " .. colorscheme.default_color_scheme)
    end,
  },
}
