local M = {}

---@param setup_type '"jorge"' | '"safe"'
M.setup_lazy = function(setup_type)
  local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
  if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
      "git",
      "clone",
      "--filter=blob:none",
      "https://github.com/folke/lazy.nvim.git",
      "--branch=stable", -- latest stable release
      lazypath,
    })
  end
  vim.opt.runtimepath:prepend(lazypath)

  require("jorge.config")
  require("lazy").setup({
    spec = {
      { import = setup_type .. ".plugins" },
    },
    checker = { enabled = true }
  })
end

return M
