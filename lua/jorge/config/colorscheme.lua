local M = {}

--- @alias theme "'dark'" | "'light'"

--- @return theme
M.get_linux_theme = function()
  local desktop = os.getenv("XDG_CURRENT_DESKTOP")
  if desktop == "GNOME" then
    local res = io.popen("gsettings get org.gnome.desktop.interface color-scheme", "r")
    if res == nil then
      return "dark"
    end
    for line in res:lines() do
      if string.match(line, "dark") then
        return "dark"
      elseif string.match(line, "default") then
        return "light"
      elseif string.match(line, "light") then
        return "light"
      end
    end
  end
  return "dark"
end

--- @return theme
M.get_windows_theme = function()
  local res = io.popen([[reg query HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize /v AppsUseLightTheme]], "r")
  if res == nil then
    return "dark"
  end
  for line in res:lines() do
    if string.match(line, "AppsUseLightTheme") then
      if string.match(line, "0x1") then
        return "light"
      elseif string.match(line, "0x0") then
        return "dark"
      end
    end
  end

  return "dark"
end

M.default_color_scheme = "carbonfox"
M.default_transparent_state = false

return M
