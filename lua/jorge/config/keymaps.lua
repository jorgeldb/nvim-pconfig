local M = {}
local opts = { noremap = true, silent = true }

-- Shorten function name
local keymap = vim.keymap.set

-- Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Delete character forward
keymap("i", "<C-L>", "<Del>", opts)

-- Shortcuts to system and black-hole registers
keymap({ "n", "x" }, "X", '"_X', opts)
keymap({ "n", "x" }, "S", '"_S', opts)
keymap({ "n", "x" }, "x", '"_x', opts)
keymap({ "n", "x" }, "s", '"_s', opts)
keymap({ "n", "x" }, "c", '"_c', opts)
keymap({ "n", "x" }, "C", '"_C', opts)
keymap({ "n", "x" }, "<leader>d", '"_d', opts)
keymap({ "n", "x" }, "<leader>D", '"_D', opts)
keymap({ "n", "x" }, "<leader>y", '"+y', opts)
keymap({ "n", "x" }, "<leader>Y", '"+Y', opts)
keymap({ "n", "x" }, "<leader>p", '"+p', opts)
keymap({ "n", "x" }, "<leader>P", '"+P', opts)

-- Change, delete, yank or visual inside / around pipes
keymap("n", "<leader>di|", 'T|"_d,', opts)
keymap("n", "<leader>da|", 'F|"_d,', opts)
keymap("n", "<leader>yi|", 'T|"+y,', opts)
keymap("n", "<leader>ya|", 'F|"+y,', opts)

keymap("n", "ci|", 'T|"_c,', opts)
keymap("n", "ca|", 'F|"_c,', opts)
keymap("n", "di|", "T|d,", opts)
keymap("n", "da|", "F|d,", opts)
keymap("n", "yi|", "T|y,", opts)
keymap("n", "ya|", "F|y,", opts)
keymap("n", "vi|", "T|v,", opts)
keymap("n", "va|", "F|v,", opts)

-- Toggle line wrap
keymap("n", "<leader>tw", "<cmd>set wrap!<cr>", opts)

-- Toggle relative line numbers
keymap("n", "<leader>tl", "<cmd>set rnu!<cr>", opts)

-- Toggle conceal at buffer level
keymap("n", "<leader>tc", function()
  ---@diagnostic disable-next-line: undefined-field
  if vim.opt_local.conceallevel:get() == 0 then
    vim.opt_local.conceallevel = 2
  else
    vim.opt_local.conceallevel = 0
  end
end, opts)

-- Resize window using <ctrl> arrow keys
keymap("n", "<C-Up>", "<cmd>resize +2<cr>", opts)
keymap("n", "<C-Down>", "<cmd>resize -2<cr>", opts)
keymap("n", "<C-Left>", "<cmd>vertical resize -2<cr>", opts)
keymap("n", "<C-Right>", "<cmd>vertical resize +2<cr>", opts)

-- Buffer navigation
keymap("n", "[b", "<cmd>bprevious<cr>", opts)
keymap("n", "]b", "<cmd>bnext<cr>", opts)
keymap("n", "<leader>bp", "<cmd>bprevious<cr>", opts)
keymap("n", "<leader>bn", "<cmd>bnext<cr>", opts)

-- Move text up and down
keymap("x", "<S-j>", ":m '>+1<CR>==gv", opts)
keymap("x", "<S-k>", ":m '<-2<CR>==gv", opts)

-- Buffer deletion
keymap("n", "<leader>bd", "<cmd>bd<cr>", opts)
keymap("n", "<leader>bD", "<cmd>bd!<cr>", opts)

keymap("n", "<leader>bc", "<cmd>%bd|e#|bd#<cr>", opts)
keymap("n", "<leader>bC", "<cmd>%bd!|e#|bd#<cr>", opts)

-- Better indenting
keymap("x", "<", "<gv", opts)
keymap("x", ">", ">gv", opts)

-- Delete whitespace
keymap("n", "<leader>cw", [[<cmd>%s/\s\+$//e<cr>]], opts)
keymap("x", "<leader>cw", [[<cmd>s/\s\+$//e<cr>]], opts)

-- Change file encoding
keymap("n", "<leader>ce", [[<cmd>set nobomb<cr><cmd>set fileencoding=utf-8<cr>]], opts)

-- Toggle shiftwidth at a local level
keymap("n", "<leader>cs", function()
  ---@diagnostic disable-next-line: undefined-field
  if (vim.opt_local.shiftwidth:get() == 4) then
    vim.opt_local.shiftwidth = 2
  ---@diagnostic disable-next-line: undefined-field
  elseif (vim.opt_local.shiftwidth:get() == 2) then
    vim.opt_local.shiftwidth = 4
  end
end, opts)

-- File tree
M.set_nvim_tree_keymaps = function()
  keymap("n", "<leader>ee", "<cmd>NvimTreeToggle<cr>", opts)
  keymap("n", "<leader>ef", "<cmd>NvimTreeFindFile<cr>", opts)
end

-- Illuminate
M.set_illuminate_keymaps = function()
  keymap("n", "]r", "<a-n>", { noremap = false, silent = true })
  keymap("n", "[r", "<a-p>", { noremap = false, silent = true })
end

-- Surround
M.get_surround_mappings = function()
  return {
    add = "<leader>sa",
    delete = "<leader>sd",
    find = "<leader>sf",
    find_left = "<leader>sF",
    highlight = "<leader>sh",
    replace = "<leader>sr",
    update_n_lines = "<leader>sn",
  }
end

M.set_surround_mappings = function ()
  keymap("x", "<leader>sa", "<Nop>", opts)
  keymap("x", "<leader>s", ":<C-u>lua MiniSurround.add(\"visual\")<cr>", opts)
end

-- LSP
M.set_lsp_keymaps = function()
  -- Navigate diagnostics
  keymap("n", "[d", function()
    vim.diagnostic.jump({ diagnostic = vim.diagnostic.get_prev(), float = true })
  end, opts)
  keymap("n", "]d", function()
    vim.diagnostic.jump({ diagnostic = vim.diagnostic.get_next(), float = true })
  end, opts)

  keymap({ "n", "x" }, "grf", function()
    vim.lsp.buf.format({ timeout_ms = 5000 })
  end, opts)

  -- Navigate LSP with telescope
  keymap("n", "grd", "<cmd>Telescope lsp_definitions<cr>", opts)
  keymap("n", "grr", "<cmd>Telescope lsp_references<cr>", opts)
  keymap("n", "gry", "<cmd>Telescope lsp_type_definitions<cr>", opts)

  -- Hover (with borders)
  -- keymap("n", "<S-k>", function() vim.lsp.buf.hover({ border = { "╔", "═", "╗", "║", "╝", "═", "╚", "║" }}) end, opts)
  keymap("n", "<S-k>", function() vim.lsp.buf.hover({ border = "double" }) end, opts)
end

M.get_lsp_mappings = function()
  local cmp = require("cmp")
  return cmp.mapping.preset.insert({
    ["<C-n>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
    ["<C-p>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
    ["<C-b>"] = cmp.mapping.scroll_docs(-4),
    ["<C-f>"] = cmp.mapping.scroll_docs(4),
    ["<C-y>"] = cmp.mapping.complete(),
    ["<C-e>"] = cmp.mapping.abort(),
    ["<CR>"] = cmp.mapping.confirm({ select = false }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    ["<S-CR>"] = cmp.mapping.confirm({
      behavior = cmp.ConfirmBehavior.Replace,
      select = false,
    }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
  })
end

M.set_luasnip_keymaps = function()
  local lsnip = require("luasnip")
  keymap({ "i", "s" }, "<C-j>", function() lsnip.jump( 1) end, opts)
  keymap({ "i", "s" }, "<C-k>", function() lsnip.jump(-1) end, opts)
end

M.get_telescope_keymaps = function()
  return {
    -- Telescope Files
    { "<leader>fF", function() require("telescope.builtin").find_files() end, desc = "Find in files" },
    { "<leader>ff", function() require("telescope.builtin").find_files({ no_ignore = true, no_ignore_parent = true, hidden = true }) end, desc = "Find in all files" },
    { "<leader>fG", function() require("telescope.builtin").live_grep() end, desc = "Grep in files" },
    { "<leader>fg", function() require("telescope.builtin").live_grep({ additional_args = { "--hidden" } }) end, desc = "Grep in all files" },

    -- Telescope text
    { "<leader>fw", function() require("telescope.builtin").grep_string() end, desc = "Grep in current buffer" },
    { "<leader>fs", require("telescope.builtin").current_buffer_fuzzy_find, desc = "Fuzzy find current buffer" },

    -- Telescope Vim
    { "<leader>fc", require("telescope.builtin").colorscheme, desc = "Change colorscheme" },
    { "<leader>fb", require("telescope.builtin").buffers, desc = "Find buffer" },
  }
end

M.get_telescope_mappings = function()
  local telescope_actions = require("telescope.actions")
  return {
    n = {
      ["<C-c>"] = telescope_actions.close,
    },
  }
end

M.get_term_keymaps = function()
  return {
    -- Terminal and lazygit
    {
      "<leader>tg",
      [[<cmd>lua require("jorge.config.terminal").lazygit_toggle()<cr>]],
      desc = "Open Lazygit terminal",
    },
    { "<leader>th", "<cmd>ToggleTerm direction=horizontal<cr>", desc = "Open horizontal terminal" },
    { "<leader>tf", "<cmd>ToggleTerm direction=float<cr>", desc = "Open floating terminal" },
    { [[<C-\>]], "<cmd>ToggleTerm<cr>", desc = "Toggle terminal" },

    -- Send to Terminal
    { "<leader>ts",
      "<cmd>ToggleTermSendCurrentLine<cr>",
      desc = "Send current line to terminal",
    },
    {
      "<leader>ts",
      "<cmd>ToggleTermSendVisualLines<cr>",
      desc = "Send current selection to terminal",
      mode = "x",
    },
  }
end

M.set_term_keymaps = function()
  keymap("t", "<esc>", [[<C-\><C-n>]], opts)
  keymap("t", "<C-h>", [[<Cmd>wincmd h<CR>]], opts)
  keymap("t", "<C-j>", [[<Cmd>wincmd j<CR>]], opts)
  keymap("t", "<C-k>", [[<Cmd>wincmd k<CR>]], opts)
  keymap("t", "<C-l>", [[<Cmd>wincmd l<CR>]], opts)
end
M.set_term_keymaps()

-- For lazygit terminal
M.unset_term_keymaps = function()
  local term_opts = { buffer = 0 }
  keymap("t", "<esc>", "<esc>", term_opts)
  keymap("t", "<C-h>", "<C-h>", term_opts)
  keymap("t", "<C-j>", "<C-j>", term_opts)
  keymap("t", "<C-k>", "<C-k>", term_opts)
  keymap("t", "<C-l>", "<C-l>", term_opts)
end

M.get_iron_keymaps = function()
  return {
    visual_send = "<leader>rs",
    send_line = "<leader>rs",
  }
end

M.get_oil_keymaps = function()
  return {
    ["g?"] = "actions.show_help",
    ["<CR>"] = "actions.select",
    ["<leader>v"] = "actions.select_vsplit",
    ["<leader>s"] = "actions.select_split",
    ["<C-t>"] = "actions.select_tab",
    ["<C-p>"] = "actions.preview",
    ["<C-c>"] = "actions.close",
    ["<C-l>"] = "actions.refresh",
    ["-"] = "actions.parent",
    ["_"] = "actions.open_cwd",
    ["`"] = "actions.cd",
    ["~"] = "actions.tcd",
    ["gs"] = "actions.change_sort",
    ["gx"] = "actions.open_external",
    ["g."] = "actions.toggle_hidden",
    ["g\\"] = "actions.toggle_trash",
  }
end

M.set_codeium_keymaps = function()
  local codeium_opts = { silent = true, noremap = true, expr = true }
  -- stylua: ignore start
  keymap("i", "<C-g>", function() return vim.fn["codeium#Accept"]() end, codeium_opts)
  keymap("i", "<C-f>", function() return vim.fn["codeium#CycleOrComplete"]() end, codeium_opts)
  keymap("i", "<C-b>", function() return vim.fn["codeium#CycleCompletions"](-1) end, codeium_opts)
  keymap("i", "<C-x>", function() return vim.fn["codeium#Clear"]() end, codeium_opts)
  keymap("n", "<leader>cc", "<cmd>CodeiumEnable<cr>", opts)
  -- stylua: ignore end
end

M.set_harpoon_keymaps = function()
  keymap("n", "<leader>ba", function() require("harpoon"):list():add() end, opts)
  keymap("n", "<leader>bm", function()
    require("harpoon").ui:toggle_quick_menu(require("harpoon"):list())
  end, opts)
  for idx = 1, 9 do
    keymap("n", "<leader>" .. idx, function() require("harpoon"):list():select(idx) end, opts)
  end
  keymap("n", "<leader>0", function() require("harpoon"):list():select(10) end, opts)
end

M.set_fold_keymaps = function()
  local f = require("fold-cycle")
  keymap("n", "zC", function() f.close_all() end, opts)
end

M.set_rgba_keymaps = function()
  keymap("n", "<leader>q", function()
    require("hex2rgba").hex2rgba()
  end, opts)
end

M.set_rgba_keymaps()

return M
