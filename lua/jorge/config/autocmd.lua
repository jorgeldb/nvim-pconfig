local ft_configs = require("jorge.config.filetype_configs")
local helpers = require("jorge.helpers")

for ft, options in pairs(ft_configs) do
  local curr_callback = function ()
    for option, value in pairs(options) do
      vim.opt_local[option] = value
    end
  end

  vim.api.nvim_create_autocmd(
    { "Filetype" },
    {
      pattern = { ft },
      callback = curr_callback,
    }
  )
end

vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
  pattern = { "*.xaml", "*.axaml" },
  callback = function()
    vim.api.nvim_command("setfiletype xml")
  end,
})

vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
  pattern = {
    vim.fn.expand("$HOME") .. "/.bash_*",
  },
  callback = function()
    vim.api.nvim_command("setfiletype sh")
  end,
})

vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
  pattern = "*.zig.zon",
  callback = function()
    vim.api.nvim_command("setfiletype zigzon")
  end,
})

vim.api.nvim_create_autocmd("InsertLeave", {
  callback = function()
    vim.opt_local.foldmethod = "expr"
  end,
})

vim.api.nvim_create_autocmd("ColorScheme", {
  callback = function()
    vim.api.nvim_command("hi Keyword cterm=italic gui=italic")
    vim.api.nvim_command("hi Conditional cterm=italic gui=italic")
    vim.api.nvim_command("hi Statement cterm=italic gui=italic")
    vim.api.nvim_command("hi PreProc cterm=italic gui=italic")
    vim.api.nvim_command("hi Type cterm=italic gui=italic")

    vim.api.nvim_command("hi Comment cterm=italic gui=italic")

    ---@diagnostic disable-next-line: param-type-mismatch
    vim.api.nvim_set_hl(0, "xmlTagName", vim.api.nvim_get_hl(0, {name = "Keyword",link = true}))
    ---@diagnostic disable-next-line: param-type-mismatch
    vim.api.nvim_set_hl(0, "xmlAttrib", vim.api.nvim_get_hl(0, {name = "Keyword",link = false}))
    ---@diagnostic disable-next-line: param-type-mismatch
    vim.api.nvim_set_hl(0, "htmlTagName", vim.api.nvim_get_hl(0, {name = "Keyword",link = true}))
    ---@diagnostic disable-next-line: param-type-mismatch
    vim.api.nvim_set_hl(0, "htmlAttrib", vim.api.nvim_get_hl(0, {name = "Keyword",link = false}))
  end,
})

vim.api.nvim_create_autocmd("DirChanged", {
  callback=helpers.send_osc7_term_sequence,
})

vim.api.nvim_create_autocmd("TextYankPost", {
  desc = "Highlight when yanking text",
  callback = function()
    vim.highlight.on_yank()
  end,
})

vim.api.nvim_create_autocmd("VimLeave", {
  callback = function()
    vim.opt.guicursor = "a:ver90blinkon500blinkoff500"
  end
})
