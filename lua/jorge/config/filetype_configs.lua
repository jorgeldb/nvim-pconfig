return {
  gdscript = {
    expandtab = false,
  },
  gleam = {
    shiftwidth = 2,
    tabstop = 2,
  },
  go = {
    expandtab = false,
  },
  json = {
    shiftwidth = 2,
    tabstop = 2,
  },
  jsonc = {
    shiftwidth = 2,
    tabstop = 2,
  },
  lua = {
    shiftwidth = 2,
    tabstop = 2,
  },
  markdown = {
    textwidth = 80,
    colorcolumn = "+1",
    shiftwidth = 2,
    tabstop = 2,
  },
  org = {
    conceallevel = 2,
    textwidth = 80,
    colorcolumn = "+1",
    shiftwidth = 1,
    tabstop = 1,
  }
}
