local M = {}
local term = require("toggleterm.terminal").Terminal
local keymaps = require("jorge.config.keymaps")
local lazygit = term:new({
  cmd = "lazygit",
  hidden = true,
  direction = "float",
  close_on_exit = true,
  on_open = function(terminal)
    vim.api.nvim_buf_set_keymap(terminal.bufnr, "n", "q", "<cmd>close<cr>", { noremap = true, silent = true })
    keymaps.unset_term_keymaps()
  end,
  float_opts = {
    border = "double",
    winblend = 0,
    highlights = {
      border = "Normal",
      background = "Normal",
    },
  },
})

function M.lazygit_toggle()
  lazygit:toggle()
end

return M
