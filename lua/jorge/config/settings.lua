-- GUI sizes
vim.opt.cmdheight = 2
vim.opt.numberwidth = 2
vim.opt.pumheight = 10

-- GUI settings
vim.opt.cursorline = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.signcolumn = "yes"
vim.opt.showmode = true
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.termguicolors = true
vim.opt.showtabline = 0
vim.opt.completeopt = { "menuone", "noselect" }

-- Editor Settings
vim.opt.conceallevel = 0
vim.opt.fileencoding = "utf-8"
vim.opt.ignorecase = true
vim.opt.hlsearch = true
vim.opt.mouse = ""
vim.opt.list = true
vim.opt.listchars = { eol = "↲", tab = "‣ " }

vim.opt.scrolloff = 8
vim.opt.sidescrolloff = 8
vim.opt.wrap = false
vim.opt.linebreak = true
vim.opt.breakindent = true

-- Tab settings
vim.opt.tabstop = 4
vim.opt.expandtab = true
vim.opt.shiftround = true
vim.opt.shiftwidth = 4
vim.opt.smartcase = true
vim.opt.smartindent = true
vim.cmd("filetype indent on")

-- Other Settings
vim.opt.updatetime = 300
vim.opt.writebackup = false
vim.opt.swapfile = false
vim.opt.undofile = true
vim.g.markdown_recommended_style = 0
vim.g.skip_ts_context_commentstring_module = true
vim.g.zig_fmt_autosave = 0

-- Lsp floating hover config
vim.diagnostic.config({ float = { border = "double" } })

-- Treesitter folding
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "v:lua.vim.treesitter.foldexpr()"
vim.opt.foldtext = "getline(v:foldstart)"
vim.opt.foldlevel = 99
vim.opt.foldenable = true

-- Font config (only for Neovide)
vim.opt.guifont = "JorgeMono Nerd Font:h12"

if (vim.loop.os_uname().sysname == "Windows_NT") then
  vim.opt.shell = "powershell"
  vim.opt.shellcmdflag = "-command"
  vim.opt.shellquote = "\""
  vim.opt.shellxquote = ""
end
