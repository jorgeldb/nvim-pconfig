local M = {}

---@param str string
local percent_encoding = function(str)
  local byte = string.byte(str)
  return string.format("%%%02X", byte)
end

---@param str string
-- Encodes a file path according to RFC 3986 Standard
local url_encode = function(str)
  if str then
    str = str:gsub("\n", "\r\n")
    str = str:gsub("([^%w %-%_%.%~])", percent_encoding)
    str = str:gsub(" ", "+")
    return str
  end
end

M.send_osc7_term_sequence = function()
  local hostname = vim.loop.os_gethostname()
  local cwd = vim.fn.getcwd()
  local encoded_cwd = url_encode(cwd)

  if encoded_cwd then
    -- The "\027" in lua represents a decimal escaped value, which is equal to the "\033" octal
    -- escape value found in wezterm's documentation:
    -- https://wezfurlong.org/wezterm/shell-integration.html#osc-7-escape-sequence-to-set-the-working-directory
    -- https://www.lua.org/manual/5.4/manual.html#3.1
    local osc7_sequence = string.format("\027]7;file://%s/%s\027\\", hostname, encoded_cwd)
    io.write(osc7_sequence)
  end
end

return M
