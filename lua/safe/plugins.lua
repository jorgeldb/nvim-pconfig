return {
  require("jorge.plugins.code"),
  require("jorge.plugins.colorscheme"),
  require("jorge.plugins.editor"),
  require("jorge.plugins.explorer"),
  require("jorge.plugins.treesitter"),
  require("jorge.plugins.ui"),
}
